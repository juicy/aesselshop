var carusel, popups;

$.stars = function() {
  var stars;
  stars = $('.stars');
  return $(stars).each(function() {
    var stars_box;
    stars_box = this;
    $(this).find('a').hover(function() {
      $(stars_box).find('a').removeClass('active');
      return $(this).addClass('active').prevAll().addClass('active');
    });
    return $(stars_box).mouseleave(function() {
      $(stars_box).find('a').removeClass('active');
      return $(stars_box).find('.fixed-active').addClass('active').prevAll().addClass('active');
    });
  });
};

carusel = function(block, in_window, width, left, right, wrap, time, points, napr, el, ei) {
  var auto, hover, max, th, to;
  if (el == null) {
    el = 'ul li';
  }
  if (ei == null) {
    ei = "%";
  }
  th = 0;
  max = $(block).find(el).length - in_window;
  hover = false;
  auto = function() {
    if (!hover) {
      to(th + 1);
    }
    return setTimeout((function() {
      return auto();
    }), time);
  };
  to = function(num) {
    if (num < 0) {
      num = max;
    }
    if (num > max) {
      num = 0;
    }
    if (napr === "top") {
      $(block).find(wrap).animate({
        "margin-top": num * -1 * width
      }, 500, function() {
        return th = num;
      });
    }
    if (napr === "left") {
      $(block).find(wrap).animate({
        "margin-left": num * -1 * width + ei
      }, 500, function() {
        return th = num;
      });
    }
    if (points) {
      $(block).find("" + points + " a").removeClass("active").addClass("passive");
      return $(block).find("" + points + " a:eq(" + num + ")").removeClass("passive").addClass("active");
    }
  };
  setTimeout((function() {
    return auto();
  }), time);
  $(block).hover((function() {
    return hover = true;
  }), function() {
    return hover = false;
  });
  $(block).find(left).click(function() {
    to(th - 1);
    return false;
  });
  $(block).find(right).click(function() {
    to(th + 1);
    return false;
  });
  if (points) {
    return $(block).find("" + points + " a").click(function() {
      var n;
      n = $(this).prevAll().length;
      to(n);
      return false;
    });
  }
};

popups = function() {
  var close, dp, open, pops, pops_close, pops_open, wind;
  dp = '[data-role="popup"]';
  pops = $(dp);
  pops_open = $('[data-role="popup_open"]');
  pops_close = $('[data-role="popup_close"]');
  wind = '[data-role="popup_window"]';
  $(document).keyup(function(e) {
    if (e.keyCode === 27 && $(".open" + dp).length > 0) {
      return close(".open" + dp);
    }
  });
  open = function(pop) {
    if ($(".open" + dp).length > 0) {
      close($(".open" + dp));
    }
    window.location.hash = pop.replace('#', '');
    $(pop).css('display', 'block');
    $(pop).addClass('open');
    return $('body').css('overflow', 'hidden');
  };
  close = function(pop) {
    history.pushState('', document.title, window.location.pathname);
    $(pop).css('display', 'none');
    $(pop).removeClass('open');
    return $('body').css('overflow', 'auto');
  };
  $('body').click(function() {
    if ($(".open" + dp).length > 0) {
      return close(".open" + dp);
    }
  });
  $(pops).each(function() {
    var hash, id;
    id = $(this).attr('id');
    hash = window.location.hash.replace('#', '');
    if (hash !== '' && hash === id) {
      open("#" + id);
    }
    return $(this).find(wind).click(function(e) {
      return e.stopPropagation();
    });
  });
  $(pops_open).each(function() {
    return $(this).click(function() {
      var target;
      target = $(this).attr('data-target');
      if ($(target).length > 0) {
        open(target);
      }
      return false;
    });
  });
  return $(pops_close).each(function() {
    return $(this).click(function() {
      var target;
      target = $(this).parents(dp);
      if ($(target).length > 0) {
        close(target);
      }
      return false;
    });
  });
};

popups();

carusel('.carusel', 1, 790, '', '', 'ul', 3000, '.points', 'left', 'ul li', 'px');

$.stars();

$('select').customSelect();
